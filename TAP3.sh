
#!/bin/bash

red="\e[0;31m"
green="\e[0;32m"
off="\e[0m"
blue="\e[0;49;34m"

function banner() {
clear
printf "\033[1;31m                                 _____ _                  _    ____ _____       _     _             \e[0m\n";
printf "\033[1;31m                                |_   _| |__   ___        / \  / ___| ____|_ __ (_) __| | ___ _ __  \e[0m\n";
printf "\033[1;31m                                  | | | '_ \ / _ \_____ / _ \| |   |  _| | '_ \| |/ _  |/ _ \ '__|    \e[0m\n";
printf "\033[1;31m                                  | | | | | |  __/_____/ ___ \ |___| |___| |_) | | (_| |  __/ |     \e[0m\n";
printf "\033[1;31m                                  |_| |_| |_|\___|    /_/   \_\____|_____| .__/|_|\__,_|\___|_|  \e[0m\n";
printf "\033[1;31m                                                                         |_|                     \e[0m\n";
printf "                                                                                                    ";
printf "\e[0;49;97m                                                                       [Version 1.1.0]                     \e[0m\n";
printf "\e[0;32m                                                  Presented By:-                     \e[0m\n";
printf "\e[4;49;97m\e[45m                                             TAPs - The Ace Programmings                                              \e[0m\n";
printf "\e[1;92m                                                                                     \e[0m\n";
}

#Setting commands
#for linux
function linux() {
  echo
  printf "$red [$green*$red]$off \033[0;49;92mChecking for requirements(automatically install, if not installed!)...\e[0m\n";
  sudo apt-get install busybox
  sleep 2
  echo
  printf "$red [$green*$red]$off \033[0;49;92mStarting spoofing mode,please wait ...\e[0m\n";
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mYour current mac address is ...\e[0m\n";
  sudo busybox ip link show eth0
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mEnter the victim's mac address to be spoofed...\e[0m\n";
  read mac_addrs   
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mProcessing...\e[0m\n";
  sleep 1
  printf "$red [$green*$red]$off \033[0;49;92mConfirming mac address....\e[0m\n";
  sleep 1
  printf "$red [$green*$red]$off \033[0;49;92mGetting mac address...\e[0m\n";
  sudo busybox ifconfig eth0 hw ether $mac_addrs
  sleep 4
  echo
  printf "$red [$green*$red]$off \033[0;49;92mSpoofing completed successfully!\e[0m\n"; 
  sleep 1
  echo
  printf "$red [$green*$red]$off \033[0;49;92mNow your mac address is...\e[0m\n";
  sudo busybox ip link show eth0
  echo
}
#for termux

function termux() {
  echo
  printf "$red [$green*$red]$off \033[0;49;92mChecking for requirements(automatically install, if not installed!)...\e[0m\n";
  pkg install busybox
  sleep 2
  echo
  printf "$red [$green*$red]$off \033[0;49;92mStarting spoofing mode,please wait ...\e[0m\n";
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mYour current mac address is ...\e[0m\n";
  tsu busybox ip link show eth0
  tsu busybox iplink show eth0
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mEnter the victim's mac address to be spoofed...\e[0m\n";
  read mac_addrs   
  sleep 3
  echo
  printf "$red [$green*$red]$off \033[0;49;92mProcessing...\e[0m\n";
  sleep 1
  printf "$red [$green*$red]$off \033[0;49;92mConfirming mac address....\e[0m\n";
  sleep 1
  printf "$red [$green*$red]$off \033[0;49;92mGetting mac address...\e[0m\n";
  tsu busybox ifconfig eth0 hw ether $mac_addrs
  sleep 4
  echo
  printf "$red [$green*$red]$off \033[0;49;92mSpoofing completed!\e[0m\n"; 
  sleep 1
  echo
  printf "$red [$green*$red]$off \033[0;49;92mNow your mac address is...\e[0m\n";
  tsu busybox ip link show eth0
  echo
}
if [ -d "/data/data/com.termux/files/usr/" ]; then
banner
echo -e "$red [$green+$red]$off ACEpider is starting.please wait...";
termux
elif [ -d "/usr/bin/" ];then
banner
printf "$red [$green*$red]$off \033[0;49;92mACEpider is starting. please wait...\e[0m\n";
sleep 3
linux
fi

